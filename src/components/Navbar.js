import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import '../index.css';
class Navbar extends Component {
    render() {
        return (
            <div>
               <nav>
                   <h3>You are select</h3>
                   <ul>
                       <li>
                           <NavLink to='/sign-in'>Sign in</NavLink>
                       </li>
                       <li>
                           <NavLink to='/sign-up'>Sign up</NavLink>
                       </li>
                   </ul>
               </nav>
            </div>
        );
    }
}

export default Navbar;