import React from 'react';
import '../index.css'
const Link = (props) => {
    const {url, url_c,title} = props.data;
    return (
        <p>
            <a href={url} className={url_c} onClick= "clickHref"> {title}</a>
        </p>
    )
}
export default Link;