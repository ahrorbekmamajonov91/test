import React, {Component} from 'react';
const list = {value: ''};
class NameForm extends Component {
    constructor(props) {
        super(props);
        this.state = list;

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }
    handleReset = e =>{
        e.preventDefault();
        this.setState(list);
    }
    handleSubmit(event) {
        alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
    }
    render() {
        const {value} = this.state;
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Name:
                        <input type="text" value={value} onChange={this.handleChange} />
                    </label>
                    <input type="submit" value="Submit" />
                    <input type="reset" value="Reset" onClick={this.handleReset}/>
                </form>
            </div>
        );
    }
}

export default NameForm;