import React, {Component} from 'react';

const list = {
    fullName: '',
    email: '',
    password: '',
};

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = list;
    }

    changeHandler = e => {
        e.preventDefault();
        const {name, value} = e.target;
        this.setState({[name]: value});
    }
    formHandler = e => {
        e.preventDefault();
       let  data = this.state;
        console.log(data['fullName'])
    }
    render() {
        const {fullName, password, email} = this.state;
        return (
            <div>
                <h1>Sing up</h1>
                <form action="" onSubmit={this.formHandler}>
                    <label htmlFor="name">Username:</label><br/>
                    <input
                        type="text"
                        name="fullName"
                        placeholder="Your name"
                        value={fullName}
                        onChange={this.changeHandler}
                        /><br/>
                    <label htmlFor="name">Email:</label><br/>
                    <input
                        type="email"
                        name="email"
                        placeholder="Your email"
                        value={email}
                        onChange={this.changeHandler}
                    /><br/>
                    <label htmlFor="name">Password:</label><br/>
                    <input
                        type="password"
                        name="password"
                        placeholder="Your password"
                        value={password}
                        onChange={this.changeHandler}
                    /><br/>
                    <button type='submit'>OK</button>
                </form>
            </div>
        );
    }
}

export default SignUp;