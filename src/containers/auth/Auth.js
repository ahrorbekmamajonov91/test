import React, {Component} from 'react';
import SignIn from "./SignIn";
import SignUp from "./SignUp";
class Auth extends Component {
    constructor(props) {
        super(props);
        this.state={
            type:true
        }
    }
    changeBtn = () =>{
        this.setState({type:!this.state.type});
    }
    render() {
        const {type} = this.state;
        return (
            <div>
                {
                    type?
                        <SignIn/>
                        :
                        <SignUp/>
                }
                <button onClick={this.changeBtn}>Sing {type?'Up':'In'}</button>
            </div>
        );
    }
}

export default Auth;