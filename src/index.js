import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route , Switch} from 'react-router-dom';
import Navbar from "./components/Navbar";
import SignIn from "./containers/auth/SignIn";
import SignUp from "./containers/auth/SignUp";
import ErrorPage from "./components/ErrorPage";
import Footer from "./components/Footer"
import './index.css';

ReactDOM.render(
    <div className="file">
        {
            <BrowserRouter>
                <Navbar />
                <Switch>
                    <Route exact path='/sign-in' component={SignIn}/>
                    <Route exact path='/sign-up' component={SignUp}/>
                    <Route path="*" component={ErrorPage}/>
                </Switch>
                <Footer />
            </BrowserRouter>
        }
    </div>,

    document.getElementById('root')
);


